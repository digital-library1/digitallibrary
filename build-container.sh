#!/bin/bash

declare -a services=("archlinux-python" "digital-library")
# export DOCKER_REGISTRY=mywarmplace.tk:433/docker-registry
export DOCKER_REGISTRY=localhost

export COLOR_BLUE="\033[0;34m"
export COLOR_NONE="\033[0m"
export COLOR_RED="\033[0;31m"
export COLOR_GREEN="\033[0;32m"

let "total_services = 0"
let "built_services = 0"

build() {
    # Enter app directory and
    # execute build script for the app
    cd docker/$APP
    if [ $? != 0 ]; then
        return 1
    fi
    sh ./build.sh
    if [ $? != 0 ]; then
        return 1
    fi
    cd ../..
}

build_service() {
    let "total_services++"

    # First arg is the name of app that we build
    local APP=$1

    echo -e "${COLOR_BLUE}> Building container \"${APP}\"${COLOR_NONE}"

    # Export environmental variables
    export APP=$APP
    export DOCKER_REGISTRY=$DOCKER_REGISTRY
    build

    # Get exit code of the build script
    status=$?

    if [ $status != 0 ]; then
        echo -e "${COLOR_RED}> Building \"${APP}\" failed${COLOR_NONE}"
    else
        echo -e "${COLOR_GREEN}> Successfully built \"${APP}\"${COLOR_NONE}"
        let "built_services++"
    fi
}

# Builds all the services specified in "services" array in the beginning
build_all() {
    for var in ${services[@]}
    do
        build_service $var
    done

    if [ $total_services == $built_services ]; then
        echo -e "${COLOR_GREEN}"
    else
        echo -e "${COLOR_RED}"
    fi

    echo -e "Built ${built_services} out of ${total_services} services.${COLOR_NONE}"
    
    # Exit with appropriate code:
    #   - 0 if all services were built.
    #   - 1 if at least one of services failed build.
    if [ $total_services == $built_services ]; then
        exit 0
    else
        exit 1
    fi
}

### Main routine ###

# Build specified containers if arguments are given,
# all of known containers otherwise.
if [[ "$#" -eq 0 ]]
then
    echo -e "${COLOR_BLUE}> Building all${COLOR_NONE}"
    build_all
else
    for e in "$@"
    do
        if [[ ! " ${services[@]} " =~ " $e " ]]; then
            echo -e "${COLOR_RED}Service \"$e\" is unknown. Aborting. ${COLOR_NONE}"
            exit 1
        fi
    done

    for e in "$@"
    do
        build_service $e
    done
fi
