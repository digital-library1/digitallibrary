import redis
from django.contrib.auth.models import User
import os

r = redis.Redis(host=os.getenv('REDIS_HOST', 'localhost'), port=int(os.getenv('REDIS_PORT', '6379')), db=0)


# noinspection PyShadowingBuiltins
def get_username(id):
    # Get name from the Redis
    name = r.get('id')

    # If name is not cached in the Redis, fetch it from Django and put into Redis
    if name is None:
        try:
            user = User.objects.get(pk=id)
            name = user.username
        except User.DoesNotExist:
            return 'ERROR: USER DOES NOT EXIST'
        r.set(str(id), name)
        # Expire cached value in 1 hour
        r.pexpire(str(id), 3600 * 1000)

    return name
