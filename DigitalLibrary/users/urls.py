from django.urls import path
from . import views

app_name = 'users'
urlpatterns = [
    path('user_info/<int:user_id>', views.user_info, name='user_info'),
]
