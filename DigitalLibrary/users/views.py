from django.contrib.auth.models import User
from django.shortcuts import render


def user_info(request, user_id):
    try:
        user = User.objects.get(pk=user_id)
        return render(request, 'user_info.html', {'user_info': user})
    except User.DoesNotExist:
        return render(request, '404.html', {'message': 'User with given ID does not exist'})
