from django.contrib.auth.models import User
from django.db import models


class MaterialRequest(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=127)
    body = models.TextField()
    telegram_alias = models.CharField(max_length=127)

