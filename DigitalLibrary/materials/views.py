import json
import uuid
from json import JSONDecodeError

from django.contrib import messages
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from pymodm.errors import DoesNotExist

from DigitalLibrary import utils, icons
from material_api import search
from material_api.models.content import ContentFile
from material_api.models.course import Course
from material_api.models.file import File
from material_api.models.section import Section
from materials.forms import CreateCourseForm, UploadFileForm, MaterialRequestForm
from materials.models import MaterialRequest
from users import user_api


def course_list(request):
    courses = list(Course.objects.all())

    # TODO: implement group checking
    # TODO: implement recent courses

    return render(request, 'course_list.html', {'courses': courses})


def validate_course_structure(request, course_uuid):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        course = Course.objects.get({'_id': uuid.UUID(course_uuid)})

        changed = False
        for section in course.sections:
            print(section)
            if section is None:
                course.sections.remove(section)
                changed = True

        if changed:
            course.save()

        return redirect('/materials/course_page/%s' % course_uuid)
    except Course.DoesNotExist:
        return render(request, '404.html', {'message': 'Course with given UUID was not found'})


def course_page(request, course_uuid):
    try:
        course = Course.objects.get({'_id': uuid.UUID(course_uuid)})

        icons_dict = {}
        for section in course.sections:
            for content in section.contents:
                if type(content) == ContentFile:
                    file = content.file
                    if file is not None:
                        icons_dict[file.hash] = icons.getIconURL(file.extension)

        return render(request, 'course_page.html',
                      {'course': course, 'icons': icons_dict, 'edit': request.GET.get('edit', 'false')})
    except ValueError:
        return render(request, '404.html', {'message': 'Your link is corrupted, please, recheck it'})
    except Course.DoesNotExist:
        return render(request, '404.html', {'message': 'Course with given UUID was not found'})


def create_course(request):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    if request.method == 'POST':
        form = CreateCourseForm(request.POST)
        if form.is_valid():
            data = request.POST.copy()
            color1, color2 = Course.gen_color()
            name = data.get('name')
            course = Course(uuid.uuid4(), name, data.get('subtitle'), color1, color2, [
                Section(uuid.uuid4(), 'Information about ' + name, [], []),
                Section(uuid.uuid4(), 'Books: ' + name, [], []),
                Section(uuid.uuid4(), 'Videos: ' + name, [], []),
                Section(uuid.uuid4(), 'Links: ' + name, [], [])
            ])
            course.save(cascade=True)
            return redirect('/materials/course_page/%s' % course.uuid)
        else:
            messages.error(request, form.errors)
    else:
        return render(request, 'create_course.html', {'form': CreateCourseForm()})


def edit_section(request, section_uuid):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        if section_uuid == 'new':
            # We are trying to create new section

            # Check if we know which course attach this section to
            course = request.GET.get('course_uuid', None)
            if course is None:
                return render(request, '400.html',
                              {'message': {'Trying to create a new section, but no course is specified for it.'}})

            new = True
            section = Section(uuid.uuid4(), 'New section', [], [])
        else:
            # We are updating existing section
            new = False
            section = Section.objects.get({'_id': uuid.UUID(section_uuid)})

        son = section.to_son()

        print(son)

        # Custom handling of content list, since we want to include their content into response, not just _id's.
        for i, c in enumerate(son['contents']):
            if c['type'] == 'file':
                # Check if non-existing file is referenced
                if section.contents[i].file is None:
                    c['file_info'] = File(hash='', name='ERROR! FILE DOES NOT EXIST').to_son()
                else:
                    c['file_info'] = section.contents[i].file.to_son()

        section_document = json.dumps(son.to_dict(), cls=DjangoJSONEncoder) \
            .replace('\\', '\\\\').replace('<', '\\<').replace('>', '\\>')

        return render(request, 'section_editor.html',
                      {'new': new, 'section': section, 'section_document': section_document})
    except ValueError:
        return render(request, '400.html', {'message': 'Your link is corrupted, please, recheck it'})
    except Course.DoesNotExist:
        return render(request, '404.html', {'message': 'Course with given UUID was not found'})
    except Section.DoesNotExist:
        return render(request, '404.html', {'message': 'Section with given UUID was not found'})


def delete_section_from_course(request, course_uuid, section_uuid):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        course = Course.objects.get({'_id': uuid.UUID(course_uuid)})
        section = Section.objects.get({'_id': uuid.UUID(section_uuid)})
        approved = request.GET.get('approved', 'false')

        # TODO: complete this!
        if approved == 'true':
            course.sections.remove(section)
            course.save()
            return redirect('/materials/course_page/' + course_uuid)
        else:
            return render(request, 'delete_section_from_course.html', {'course': course, 'section': section})
    except ValueError:
        return render(request, '400.html', {'message': 'Your link is corrupted, please, recheck it'})
    except Course.DoesNotExist:
        return render(request, '404.html', {'message': 'Course with given UUID was not found'})
    except Section.DoesNotExist:
        return render(request, '404.html', {'message': 'Section with given UUID was not found'})


def upload_file(request):
    """
    Serves the page for file upload and accepts files sent with file upload form.

    :param request: Django request object
    :return: page with form or redirection to page with info about newly uploaded file
    """
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            # Only allow direct upload for moderators, for all other users, set "pending" flag.
            pending = not request.user.has_perm('auth:moderate')
            f = File.create_from_data(request.user.id, pending, form.cleaned_data.get('name'), request.FILES['file'])
            return redirect('/materials/file_info/%s' % f.hash)
    else:
        form = UploadFileForm()
    return render(request, 'upload_file.html', {'form': form})


def file_info(request, file_hash):
    try:
        file = File.objects.get({'_id': file_hash})
    except File.DoesNotExist:
        return render(request, '404.html', {'message': 'File with given hash was not found'})

    uploader_name = ''
    if request.user.has_perm('auth.moderate'):
        uploader_name = user_api.get_username(file.uploader)

    icon = icons.getIconURL(file.extension)

    return render(request, 'file_info.html', {'file': file, 'uploader_name': uploader_name, 'icon': icon})


def file_list(request):
    # Get page number, if any was provided by user in the "page" argument
    page_number = request.GET.get('page', 0)
    if type(page_number) == str:
        try:
            page_number = int(page_number)
        except ValueError:
            return render(request, '400.html', {'message': 'Page number should be integer number'})

    page_size = 5

    files = File.objects.skip(page_number * page_size).limit(page_size).all()

    return render(request, 'file_list.html', {'page': page_number, 'files': files})


def save_section(request, section_uuid):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    # only accept POST requests here
    if request.method != 'POST':
        return render(request, '400.html', {'message': 'Expected POST request'})

    try:
        section = json.loads(request.body)

        # Check if we are creating new section & attaching it to the course
        course_uuid = request.GET.get('course_uuid', None)
        course = None
        if course_uuid is not None:
            # We are trying to attach section to the course
            try:
                course = Course.objects.get({'_id': uuid.UUID(course_uuid)})
            except ValueError:
                return HttpResponse('non-UUID id was passed', status=400)
            except Course.DoesNotExist:
                return HttpResponse('Error: course with passed UUID does not exist', status=404)

        # Map types to pymodm classes
        for c in section['contents']:
            if c['type'] == 'text':
                c['_cls'] = 'material_api.models.content.ContentText'
            elif c['type'] == 'code':
                c['_cls'] = 'material_api.models.content.ContentCode'
            elif c['type'] == 'image':
                c['_cls'] = 'material_api.models.content.ContentImage'
            elif c['type'] == 'file':
                c['_cls'] = 'material_api.models.content.ContentFile'
            else:
                raise ValueError('Unknown content type passed')

        section = Section.from_document(section)
        section.uuid = section_uuid
        print(section)
        section.save()

        # Actually attach section to the course if user asks for this
        if course_uuid is not None:
            # Check if it is already in this course.
            # In this case, simply ignore adding.
            # If not, add section to the course.
            if section not in course.sections:
                course.sections.append(section)
                course.save()

        return HttpResponse('👌Saved')
    except JSONDecodeError:
        return HttpResponse('{"message": "%s"}' % 'Error occurred while decoding JSON', status=400)
    # TODO: add internal server error here


def save_course_name_subtitle(request, course_uuid):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        course = Course.objects.get({'_id': uuid.UUID(course_uuid)})
        new_info = json.loads(request.body)

        course.name = new_info['name']
        course.subtitle = new_info['subtitle']
        course.save()

        return HttpResponse('👌Saved')
    except JSONDecodeError:
        return HttpResponse('Passed JSON is not valid', status=400)
    except ValueError:
        return HttpResponse('Non-UUID id was passed', status=400)
    except Course.DoesNotExist:
        return HttpResponse('Course with passed UUID does not exist', status=404)


def api_fetch_file_list(request):
    # Get page number, if any was provided by user in the "page" argument
    page_number = request.GET.get('page', 0)
    if type(page_number) == str:
        try:
            page_number = int(page_number)
        except ValueError:
            return render(request, '400.html', {'message': 'Page number should be integer number'})

    page_size = 100

    files = File.objects.skip(page_number * page_size).limit(page_size).all()

    new_files = []
    for f in files:
        obj = f.to_son().to_dict()
        obj['create_date'] = utils.pretty_date(obj['create_date'])
        obj['icon'] = icons.getIconURL(f.extension)
        obj['uploader_name'] = user_api.get_username(obj['uploader'])
        new_files.append(obj)

    print(new_files)

    return HttpResponse(json.dumps(new_files))


def download(request, file_hash):
    try:
        file = File.objects.get({'_id': file_hash})
        response = HttpResponse(status=200)
        response['Content-Disposition'] = "attachment; filename=%s" % format(file.name) + \
                                          ('.' + file.extension if file.extension is not None else '')
        response['X-Accel-Redirect'] = "/protected_media/%s" % format(file.hash)
        return response
    except File.DoesNotExist:
        return render(request, '404.html', {'message': 'Requested file was not found'})


def material_request(request):
    if request.method == 'POST':
        form = MaterialRequestForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.user_id = request.user.id
            model.save()
            return redirect('/')
        else:
            messages.error(request, form.errors)
    else:
        return render(request, 'materials/material_request.html', {'form': MaterialRequestForm()})


def material_requests_list(request):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    list = MaterialRequest.objects.all()
    return render(request, 'materials/material_requests_list.html', {'list': list})


def material_request_success(request):
    return render(request, 'materials/material_request_success.html')


def remove_material_request(request, req_id):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        claim = MaterialRequest.objects.get(pk=req_id)
        claim.delete()
        return redirect('/materials/material_requests_list')
    except MaterialRequest.DoesNotExist:
        return render(request, '404.html', {'message': 'Non-existent req_id passed'})


def delete_file(request, file_hash):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        file = File.objects.get({'_id': file_hash})
        file.delete()
        return redirect('/materials/file_list')
    except File.DoesNotExist:
        return HttpResponse('File with given file_hash does not exist', status=404)


def searchbar_search(request, query):
    courses, sections, files = search.searchbar_search(query)

    icons_dict = {}
    for f in files:
        icons_dict[f[0]] = icons.getIconURL(f[2])

    return render(request, 'materials/searchbar_results.html',
                  {'courses': courses,
                   'sections': sections,
                   'files': files,
                   'icons': icons_dict
                   })


def section_page(request, section_uuid):
    try:
        section = Section.objects.get({'_id': utils.uuidJL(section_uuid)})

        icons_dict = {}
        for content in section.contents:
            if type(content) == ContentFile:
                file = content.file
                if file is not None:
                    icons_dict[file.hash] = icons.getIconURL(file.extension)

        return render(request, 'materials/section_page.html',
                      {'section': section, 'icons': icons_dict, 'edit': request.GET.get('edit', 'false')})
    except ValueError:
        return render(request, '404.html', {'message': 'Your link is corrupted, please, recheck it'})
    except Section.DoesNotExist:
        return render(request, '404.html', {'message': 'Section with given UUID was not found'})
