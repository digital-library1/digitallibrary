function save_name_subtitle() {
    const name = $('#name').html()
    const subtitle = $('#subtitle').html()

    const save_status = $('#save_status')
    save_status.html('Saving...')

    const body = JSON.stringify({
        'name': name,
        'subtitle': subtitle
    })

    HttpPost(`/materials/save_course_name_subtitle/${course_uuid}`, body,
        function (responseText) {
            save_status.html(responseText)
        },
        function (responseText) {
            save_status.html(responseText)
        }
    )
}