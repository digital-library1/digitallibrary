function humanFileSize(bytes) {
    const thresh = 1000
    if (Math.abs(bytes) < thresh) {
        return bytes + ' B'
    }
    const units = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    let u = -1
    do {
        bytes /= thresh
        ++u
    } while (Math.abs(bytes) >= thresh && u < units.length - 1)
    return bytes.toFixed(1) + ' ' + units[u]
}

function closeFilePicker() {
    $('#file-picker-content').addClass('file-picker-slide-out')
    let fp = $('#file-picker')
    fp.addClass('fade-out')
    setTimeout(() => fp.remove(), 400)
}

// Picked file represents an _id of a currently selected file
function openFilePicker(page, picked_file, callback) {
    let onSuccess = files => {
        let elementsString = ""
        for (file of files) {
            const element = `
                <div class="file-picker-entry" id="file_entry_${file._id}">
                    <img src="${file.icon}" alt="icon" style="width: 48px; height: 48px; margin: 8px 8px 8px 0;"/>
                    <div>
                        <h4 class="sans-serif file-picker-filename">${file.name}</h4>
                        <span class="sans-serif file-picker-filesize">Size: ${humanFileSize(file.size)}</span>
                        <span class="sans-serif file-picker-create-info">
                            Uploaded ${file.create_date} by 
                            <a href="/users/user_info/${file.uploader}" onclick="stopPropagation(this)" target="_blank">${file.uploader_name}</a>
                        </span>
                    </div>
                </div>
                <hr style="margin: 4px 0">
            `

            elementsString += element
        }

        $('#file-picker-list').html(elementsString)

        for (file of files) {
            let f = file
            $('#file_entry_' + file._id).click(ev => {
                closeFilePicker()
                callback(f)
            })
        }
    }

    let onFailure = response => {
        $('#file-picker-list').html(`
            <h3>Error occurred while retrieving file list</h3>
            <p>${response}</p>
        `)
    }

    HttpGet('/materials/api_fetch_file_list?page=' + page,
        responseText => {
            console.log('Response:')
            console.log(responseText)
            onSuccess(JSON.parse(responseText))
        },
        responseText => {
            console.log(responseText)
            onFailure(responseText)
        }
    )

    let picker = `
        <div id="file-picker" class="file-picker fade-in">
            <div id="file-picker-content" class="file-picker-content file-picker-slide-in">
                <div style="display: flex; justify-content: space-between; align-items: center; margin: -4px 0;">
<!--                    <span class="file-picker-header-element"><b>Pick a file</b></span>-->
                    <div></div>
                    <div class="file-picker-search-bar">
                        <img class="file-picker-search-icon" alt="Search icon" src="/static/search_icon.svg">
                        <a class="file-picker-search-text">Search a file</a>
                    </div>
                    <button class="file-picker-header-element" onclick="closeFilePicker()">Close</button>
                </div>
                <hr>
                <div id="file-picker-list">
                </div>
            </div>
        </div>
        `
    $('body').append($(picker))
}
