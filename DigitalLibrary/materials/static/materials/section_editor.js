// List that holds the order of contents inside this section. Contains IDs of materials which are used as keys for
// contents dict.
let contents_order = []
let contents = {}


/** Moves element with given local ID up. Affects both model and UI. */
function moveUp(id) {
    let index = contents_order.indexOf(id)

    if (index === -1) {
        console.log(`Something is wrong, trying to move up non-existing item ${id}`)
        return
    }
    if (index === 0) {
        console.log('Trying to move up the top-most item')
        return
    }

    // remove element from the material_order list
    let element = contents_order.splice(index, 1)[0]

    // Save jQuery objects for later
    let obj = $('#' + element)
    let upper_obj = $('#' + contents_order[index - 1])

    // Insert element at the new position
    contents_order.splice(index - 1, 0, element)

    // move element inside UI
    obj.detach()
    obj.insertBefore(upper_obj)

    // Animate the element swapping
    obj[0].animate([
        {transform: `translateY(${upper_obj.height()}px)`, easing: 'ease-out'},
        {transform: 'translateY(0px)', easing: 'ease-in'}
    ], 200)
    upper_obj[0].animate([
        {transform: `translateY(-${obj.height()}px)`, easing: 'ease-out'},
        {transform: 'translateY(0px)', easing: 'ease-in'}
    ], 200)
}

/** Moves element with given local ID down. Affects both model and UI. */
function moveDown(id) {
    let index = contents_order.indexOf(id)

    if (index === -1) {
        console.log(`Something is wrong, trying to move down non-existing item ${id}`)
        return
    }
    if (index === contents_order.length - 1) {
        console.log('Trying to move down the bottom-most item')
        return
    }

    // remove element from the material_order list
    let element = contents_order.splice(index, 1)[0]

    // Save jQuery objects for later
    let obj = $('#' + element)
    let lower_obj = $('#' + contents_order[index])

    // Insert element at the new position
    contents_order.splice(index + 1, 0, element)

    // move element inside UI
    obj.detach()
    obj.insertAfter(lower_obj)

    // Animate the element swapping
    obj[0].animate([
        {transform: `translateY(-${lower_obj.height()}px)`, easing: 'ease-out'},
        {transform: 'translateY(0px)', easing: 'ease-in'}
    ], 200)
    lower_obj[0].animate([
        {transform: `translateY(${obj.height()}px)`, easing: 'ease-out'},
        {transform: 'translateY(0px)', easing: 'ease-in'}
    ], 200)
}

function input(content_id, key, value) {
    console.log('input: ' + key + '=' + value)
    contents[content_id][key] = value
}

/** id - temporary local ID used to identify the material's UI to material itself. */
function addText(id, initial_text) {
    console.log('Adding new text with id ' + id + ' and initial text "' + initial_text + '"')
    contents[id] = {
        'type': 'text',
        'text': initial_text,
    };
    contents_order.push(id)

    let element = `<div class="material_entry" id="${id}">
                        <div style="flex-grow: 1;">
                            <h3 style="margin: 4px">Text</h3>                           
                            <span contenteditable="plaintext-only" class="input no-max-width"
                            id="content_${id}" oninput="input(${id}, 'text', this.innerHTML)"></span>
                        </div>
                        <div class="material-entry-buttons">
                            <button class="move-up-button" onclick="moveUp(${id})"><img src="/static/move_up_button.svg" alt="Move up"/></button>
                            <button class="move-down-button" onclick="moveDown(${id})"><img src="/static/move_down_button.svg" alt="Move down"/></button>
                            <a class="delete-button">Delete</a>
                        </div>
                    </div>`
    add(element)
    $('#content_' + id).text(initial_text)
}

function addCode(id, initial_text) {
    contents[id] = {
        'type': 'code',
        'code': initial_text
    }
    contents_order.push(id)

    let element = `<div class="material_entry" id="${id}">
                        <div style="flex-grow: 1;">
                            <h3 style="margin: 4px">Code</h3>                           
                                <span contenteditable="plaintext-only" class="input no-max-width monospace-text" 
                                id="content_${id}" oninput="input(${id}, 'code', this.innerHTML)"></span>
                        </div>
                        <div class="material-entry-buttons">
                            <button class="move-up-button" onclick="moveUp(${id})"><img src="/static/move_up_button.svg" alt="Move up"/></button>
                            <button class="move-down-button" onclick="moveDown(${id})"><img src="/static/move_down_button.svg" alt="Move down"/></button>
                            <a class="delete-button">Delete</a>
                        </div>
                    </div>`

    add(element)
    $('#content_' + id).text(initial_text)

    // Make TAB work for 4-space input, not skip-to-next-element
    $(`#content_${id}`).keydown(function (e) {
        if (e.keyCode === 9) { // tab was pressed
            const $this = $(this)
            let range = getSelection().getRangeAt(0)
            const start = range.startOffset
            const end = range.endOffset
            const value = $this.text()

            // set span value to: text before caret + tab + text after caret
            $this.text(value.substring(0, start) + "    " + value.substring(end))

            range.setStart(this.childNodes[0], start + 4)

            input(id, 'code', $this.text())

            // prevent the focus lose
            e.preventDefault()
        }
    })
}


// function addImage(id, initial_image) {
//
// }


function addFile(id, initial_file) {
    console.log('Adding new file with id ' + id + ' and initial file ' + initial_file)
    contents[id] = {
        'type': 'file',
        'file': (initial_file != null ? initial_file._id : null),
    };
    contents_order.push(id)

    let element = `<div class="material_entry" id="${id}">
                        <div style="flex-grow: 1;">
                            <h3 style="margin: 4px">File</h3>
                            <button id="file_${id}"></button>
                        </div>
                        <div class="material-entry-buttons">
                            <button class="move-up-button" onclick="moveUp(${id})"><img src="/static/move_up_button.svg" alt="Move up"/></button>
                            <button class="move-down-button" onclick="moveDown(${id})"><img src="/static/move_down_button.svg" alt="Move down"/></button>
                            <a class="delete-button">Delete</a>
                        </div>
                    </div>`
    add(element)

    const file_button = $('#file_' + id)
    if (initial_file != null) {
        file_button.html(initial_file.name)
    } else {
        file_button.html('Select file...')
    }
    file_button.click(ev => {
        console.log('Button clicked!')

        let _id = initial_file != null ? initial_file._id : null

        openFilePicker(0, _id, file => {
            console.log('Updating content with ' + file + '. Setting id to ' + file._id)
            file_button.html(file.name)
            contents[id]['file'] = file._id
        })
    })
}


function add(element) {
    let materialList = $('#material_list')
    materialList.append(element)
}


function assembleNewSection() {
    let section = {}

    section['name'] = $('#name').text()
    section['tags'] = $('#tags').text().replace(/\s/g, '').split(',').filter(e => e !== '')
    section['contents'] = []

    for (let local_id of contents_order) {
        content = contents[local_id]

        if (content.type === 'file' && content.file == null)
            throw "nullFile"

        section['contents'].push(content)
    }

    return JSON.stringify(section)
}


function save() {
    const save_status = $('#save_status')

    let section;
    try {
        section = assembleNewSection()
    } catch (e) {
        if (e === 'nullFile') {
            save_status.html('⚠️ Can\'t save null file! Recheck your content')
            return
        }
    }

    console.log(section)

    save_status.html('Saving...')
    let course_uuid = getURLParam('course_uuid')

    HttpPost('/materials/save_section/' + section_uuid + (course_uuid ? '?course_uuid=' + course_uuid : ''), section,
        function (responseText) {
            save_status.html(responseText)
        },
        function (responseText) {
            save_status.html(responseText)
        },
    )
}


function load(str) {
    let section = JSON.parse(str)

    $('#name').html(section.name)
    $('#tags').html(section.tags.join(', '))

    for (let content of section.contents) {
        switch (content.type) {
            case 'text':
                addText(genLocalContentID(), content.text)
                break
            case 'code':
                addCode(genLocalContentID(), content.code)
                break
            case 'image':
                // TODO: complete me!
                addImage(genLocalContentID())
                break
            case 'file':
                addFile(genLocalContentID(), content.file_info)
                break
            default:
                console.log('WARNING: unknown content type')
        }
    }

    // Expand all textareas to fit their contents
    $("textarea").each(function (textarea) {
        let $this = $(this)
        $this.height($this[0].scrollHeight)
    })
}


/** Generates a random material ID which is used locally by page to connect */
function genLocalContentID() {
    return Math.floor(Math.random() * 1000000)
}

