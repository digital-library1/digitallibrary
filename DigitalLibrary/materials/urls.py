from django.urls import path
from . import views

app_name = 'materials'
urlpatterns = [
    path('course_page/<str:course_uuid>', views.course_page, name='course_page'),
    path('course_list/', views.course_list, name='course_list'),
    path('create_course/', views.create_course, name='create_course'),
    path('edit_section/<str:section_uuid>', views.edit_section, name='edit_section'),
    path('delete_section_from_course/<str:course_uuid>/<str:section_uuid>', views.delete_section_from_course, name='delete_section_from_course'),
    path('upload_file/', views.upload_file, name='upload_file'),
    path('file_info/<str:file_hash>', views.file_info, name='file_info'),
    path('file_list', views.file_list, name='file_list'),
    path('save_section/<str:section_uuid>', views.save_section, name='save_section'),
    path('save_course_name_subtitle/<str:course_uuid>', views.save_course_name_subtitle, name='save_course_name_subtitle'),
    path('validate_course_structure/<str:course_uuid>', views.validate_course_structure, name='validate_course_structure'),
    path('api_fetch_file_list', views.api_fetch_file_list, name='api_fetch_file_list'),
    path('download/<str:file_hash>', views.download, name='download'),
    path('material_request', views.material_request, name='material_request'),
    path('material_request_success', views.material_request_success, name='material_request_success'),
    path('material_requests_list', views.material_requests_list, name='material_requests_list'),
    path('remove_material_request/<int:req_id>', views.remove_material_request, name='remove_material_request'),
    path('delete_file/<str:file_hash>', views.delete_file, name='delete_file'),
    path('searchbar_search/<str:query>', views.searchbar_search, name='searchbar_search'),
    path('section_page/<str:section_uuid>', views.section_page, name='section_page'),
]

