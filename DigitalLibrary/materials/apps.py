from django.apps import AppConfig


class CoursePageConfig(AppConfig):
    name = 'materials'
