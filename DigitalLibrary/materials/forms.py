from django import forms

from material_api.models.course import Course
from materials.models import MaterialRequest
from our_auth.models import SignupSubmission


class CreateCourseForm(forms.Form):
    name = forms.CharField(max_length=255, label='Course name')
    subtitle = forms.CharField(max_length=255, label='Course subtitle')


class UploadFileForm(forms.Form):
    name = forms.CharField(max_length=255)
    file = forms.FileField()


class MaterialRequestForm(forms.ModelForm):
    title = forms.CharField(max_length=127, label='Title')
    body = forms.Textarea()
    telegram_alias = forms.CharField(max_length=127, label='Telegram Alias')

    class Meta:
        model = MaterialRequest
        fields = ('title', 'body', 'telegram_alias')

