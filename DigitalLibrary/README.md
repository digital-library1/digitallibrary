# Digital Library

 ## How to run me
 First, you need to build the Digital Library container. Type
 ```shell script
./build-container.sh
```
This will build `archlinux-python` and `digital-library` containers.

Then, simply
```shell script
docker-compose up -d
```
This will start all the containers in the background.

BUT

Keep in mind that such a system have to have security concerns, so you MUST create `.env` file that contains all the required environmental variables.

Also, you have to either populate `certs` directory or replace HTTPS in `nginx-config/nginx.conf` to plain HTTP.
 
 ## I have troubles!
 First of all, IN CASE OF TROUBLES,
 ```shell script
docker stop $(docker ps -q)
docker container rm $(docker container ls -aq)
```
and remove all data directories from the root of the project.