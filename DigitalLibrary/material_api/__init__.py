from elasticsearch import Elasticsearch
from pymodm import connect
import os

connect('mongodb://%s:%s@%s:%s/materials?authSource=admin' % (
    os.getenv('MONGO_USER', 'test'),
    os.getenv('MONGO_PASSWORD', 'test'),
    os.getenv('MONGO_HOST', 'localhost'),
    os.getenv('MONGO_PORT', '27017')
))

# Initialize the Elasticsearch connection
es = Elasticsearch({
    os.getenv('ELASTICSEARCH_HOST', 'localhost'): int(os.getenv('ELASTICSEARCH_PORT', '9200'))
})
