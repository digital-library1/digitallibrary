from DigitalLibrary import utils
from material_api import es


def search(name: str, index: str = "materials", fuzziness: int = 3):
    """
    :param index: the index to search in.
    :param fuzziness: the fuzziness factor of the search.
    :param name: the field you are performing search on.
        Recommended ones are "name" (for objects which have name, such as courses, sections, files), "text" (for text
        contents in sections), "code" (for code contents in sections).
    """
    return es.search(index=index, body={
        "query": {
            "match": {
                "name": {
                    "query": name,
                    "fuzziness": fuzziness
                }
            }
        }
    })


def searchbar_search(query: str):
    """
    Searches for the courses, sections and files.
    Returns:
        for courses: (uuid, name)
        for sections: (uuid, name)
        for files: (hash, name, extensions)

    :param query: the user's entered query
    :return: all the found results that should be seen in the searchbar
    """
    # Execute the query
    res = search(query)
    print(res)

    # Create lists for storing the found results in sorted order
    courses = []
    sections = []
    files = []

    for r in res['hits']['hits']:
        if r['_type'] == 'course':
            uuid = utils.uuidJL(r['_id'])
            courses.append((uuid, r['_source']['name']))

        elif r['_type'] == 'section':
            uuid = utils.uuidJL(r['_id'])
            sections.append((uuid, r['_source']['name']))

        elif r['_type'] == 'file':
            # Don't show up pending files in the search results
            if r['_source']['pending']:
                continue
            files.append((r['_id'], r['_source']['name'], r['_source']['extension']))

    return courses, sections, files
