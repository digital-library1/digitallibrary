from pymodm import fields

from .content_base import ContentBase
from .file import File
from .image import Image


class ContentText(ContentBase):
    text = fields.CharField(required=True, blank=True)
    type = fields.CharField(default='text', required=True)

    def clean(self):
        pass


class ContentCode(ContentBase):
    code = fields.CharField(required=True, blank=True)
    type = fields.CharField(default='code', required=True)

    def clean(self):
        pass


class ContentImage(ContentBase):
    image = fields.ReferenceField(Image, required=True)
    type = fields.CharField(default='image', required=True)

    def clean(self):
        pass


class ContentFile(ContentBase):
    file = fields.ReferenceField(File, required=True)
    type = fields.CharField(default='file', required=True)

    def clean(self):
        pass
