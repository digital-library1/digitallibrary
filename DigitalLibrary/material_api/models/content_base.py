from pymodm import EmbeddedMongoModel
from pymodm.errors import ValidationError


class ContentBase(EmbeddedMongoModel):
    """
    All content subtypes should inherit from this class. This allows to organize work with pymodm (the ODM used in this
     project) in a beautiful way.
    """
    def clean(self):
        raise ValidationError("Can't use base class. Use its descendants.")
