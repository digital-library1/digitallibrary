from pymodm import fields, MongoModel, EmbeddedMongoModel


class Image(MongoModel):
    hash = fields.CharField(primary_key=True, required=True)
    name = fields.CharField(required=True)
    thumbnail = fields.BinaryField(required=True)
    width = fields.IntegerField(required=True)
    height = fields.IntegerField(required=True)
