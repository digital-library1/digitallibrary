import os
import datetime

from django.core.files.uploadedfile import InMemoryUploadedFile
from pymodm import fields, MongoModel, EmbeddedMongoModel
import hashlib
import filetype

from DigitalLibrary import settings


class File(MongoModel):
    hash = fields.CharField(primary_key=True, required=True)
    name = fields.CharField(required=True)
    extension = fields.CharField(blank=True, required=True)
    create_date = fields.DateTimeField(required=True)
    allowed_groups = fields.ListField(fields.CharField(), blank=True, required=True)
    uploader = fields.IntegerField(required=True)
    size = fields.IntegerField(required=True)
    pending = fields.BooleanField(required=True)  # If file is yet to be checked by the moderator

    @staticmethod
    def create_from_data(uploader: int, is_moderator: bool, name: str, file: InMemoryUploadedFile):
        """
        Saves the file to disk, creates new file entry in database and returns it.
        It file with such contents already exists, just return its entry.

        :rtype: File - created or retrieved file instance
        """

        # Calculate the file hash
        m = hashlib.sha1()
        for chunk in file.chunks():
            m.update(chunk)
        sha = m.hexdigest()

        # File name is just SHA1 hash sum of the file contents
        path = os.path.join(settings.MEDIA_ROOT, sha)

        # If file already exists, just ignore writing
        if not os.path.exists(path):
            # Write file contents to the file
            with open(path, 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)

            # Setup file entry attributes
            create_date = datetime.datetime.now()
            extension = filetype.guess_extension(path)

            f = File(sha, name, extension, create_date, [], uploader, file.size, is_moderator)
            f.save()
            return f

        # Try returning file. If it does not exist, then we have a file, but not an entry. Create entry and return it.
        try:
            return File.objects.get({'_id': sha})
        except File.DoesNotExist:
            create_date = datetime.datetime.now()
            extension = filetype.guess_extension(path)

            f = File(sha, name, extension, create_date, [], uploader, file.size, is_moderator)
            f.save()
            return f
