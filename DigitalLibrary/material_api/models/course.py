import random

from bson import CodecOptions, JAVA_LEGACY
from bson.binary import UUID_SUBTYPE
from pymodm import fields, MongoModel, EmbeddedMongoModel
from .section import Section
import colorsys


def _rgb_to_hex(rgb):
    return '#%02x%02x%02x' % rgb


class Course(MongoModel):
    uuid = fields.UUIDField(primary_key=True, required=True)
    name = fields.CharField(required=True)
    subtitle = fields.CharField(required=True)
    course_color_1 = fields.CharField(default='#999', required=True)
    course_color_2 = fields.CharField(default='#aaa', required=True)
    sections = fields.ListField(fields.ReferenceField(Section), blank=True, required=True)

    class Meta:
        codec_options = CodecOptions(uuid_representation=JAVA_LEGACY)

    @staticmethod
    def gen_color():
        hue = random.random()
        # Gen some colors
        color1 = colorsys.hsv_to_rgb(hue, 0.75, 0.70)
        color2 = colorsys.hsv_to_rgb(hue, 0.75, 0.50)
        print(color1, color2)
        # Convert to ints
        color1 = tuple([int(round(255 * v)) for v in color1])
        color2 = tuple([int(round(255 * v)) for v in color2])
        print(color1, color2)
        # Convert to hex
        color1 = _rgb_to_hex(color1)
        color2 = _rgb_to_hex(color2)
        print(color1, color2)
        return color1, color2
