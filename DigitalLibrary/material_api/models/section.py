from pymodm import fields, MongoModel, EmbeddedMongoModel

from .content import ContentBase


class Section(MongoModel):
    uuid = fields.UUIDField(primary_key=True, required=True)
    name = fields.CharField(required=True)
    # List of tags represented as strings
    tags = fields.ListField(fields.CharField(), blank=True)
    contents = fields.EmbeddedDocumentListField(ContentBase, blank=True)

    def __str__(self):
        return 'Section(uuid=%s, name=%s, contents=%s)' % (self.uuid, self.name, self.contents)
