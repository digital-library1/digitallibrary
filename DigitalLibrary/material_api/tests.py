import uuid

from material_api import *


def test_material_add_get_delete():
    """
        Tests the add, get and delete functions of Material class.
    """
    material1 = Material(
        uuid=uuid.uuid4(),
        name='Material 1',
        tags=[],
        contents=[]
    )
    material2 = Material(
        uuid=uuid.uuid4(),
        name='Material 2',
        tags=[],
        contents=[]
    )

    # Add part
    Material.add(material1)
    Material.add(material2)

    # Get & check part
    results = Material.get()
    assert len(results) == 2
    assert results[0].name == 'Material 1'
    assert results[1].name == 'Material 2'

    result = Material.get(results[0].uuid)
    assert result is not None
    assert type(result) == Material

    # Remove part
    Material.delete(material1.uuid)
    Material.delete(material2.uuid)

    # Check that we removed everything
    results = Material.get()
    assert len(results) == 0


def test_material_type_checking():
    failed = False
    try:
        Material.add("I'm wrong type")
    except ValueError:
        failed = True
    assert failed is True

    try:
        Material.delete("I'm wrong type")
    except ValueError:
        failed = True
    assert failed is True


test_material_add_get_delete()
