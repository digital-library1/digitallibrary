from django.http import HttpRequest
from django.shortcuts import redirect


class RequireLoginMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: HttpRequest):
        # Prohibit access to any non-auth parts for non-authorized users
        if not request.user.is_authenticated and not (
                request.path.startswith('/our_auth') or
                request.path.startswith('/auth')):
            return redirect('/auth/login')

        response = self.get_response(request)
        return response
