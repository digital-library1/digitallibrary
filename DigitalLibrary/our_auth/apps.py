from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'our_auth'

    # def ready(self):
    #     # Init groups
    #     from . import groups
    #     groups.initGroups()
