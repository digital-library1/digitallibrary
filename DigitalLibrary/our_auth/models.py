from django.db import models


class SignupSubmission(models.Model):
    username = models.CharField(max_length=255)
    telegram_alias = models.CharField(max_length=127)
    email = models.EmailField(max_length=127)
    comment = models.CharField(max_length=1023)

