from django.urls import path
from . import views

app_name = 'our_auth'
urlpatterns = [
    path('signup', views.signup, name='signup'),
    path('signups_list', views.signups_list, name='signups_list'),
    path('approve_signup/<int:id>', views.approve_signup, name='approve_signup'),
    path('decline_signup/<int:id>', views.decline_signup, name='decline_signup'),
]

