from django import forms

from our_auth.models import SignupSubmission


class SignupSubmitForm(forms.ModelForm):
    username = forms.CharField(max_length=255, label='Username')
    telegram_alias = forms.CharField(max_length=127, label='Telegram Alias')
    email = forms.EmailField(max_length=127, label='Email')
    comment = forms.CharField(max_length=1023, label='Comment')

    class Meta:
        model = SignupSubmission
        fields = ('username', 'telegram_alias', 'email', 'comment')
