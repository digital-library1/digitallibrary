import datetime
import random

from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect

from our_auth.forms import SignupSubmitForm
from our_auth.models import SignupSubmission


def signup(request):
    if request.method == 'POST':
        # Somebody is submitting the form
        form = SignupSubmitForm(request.POST)
        if form.is_valid():
            form.save()
            # TODO: redirect to page with some info
            return redirect('/')
    else:
        # Somebody asks for a form
        form = SignupSubmitForm()

    return render(request, 'registration/signup.html', {'form': form})


def signups_list(request):
    if not request.user.has_perm('auth:moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    submissions = SignupSubmission.objects.all()

    return render(request, 'signups_list.html', {'submissions': submissions})


def _gen_password():
    letters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']
    pwd = ''.join([random.choice(letters) for i in range(16)])
    return pwd


def approve_signup(request, id):
    if not request.user.has_perm('auth:moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        submission = SignupSubmission.objects.get(pk=id)

        user = User(
            username=submission.username,
            first_name=submission.telegram_alias,
            last_name='',
            email=submission.email,
            date_joined=datetime.datetime.now()
        )

        # Generate random password
        pwd = _gen_password()
        user.set_password(pwd)
        # Create newly created user
        user.save()

        # Delete submission after user was created
        submission.delete()

        # Return password, so moderator can send it to the user
        return HttpResponse('{"password": "%s"}' % pwd)
    except SignupSubmission.DoesNotExist:
        return render(request, '404.html', {'message': 'Submission you are trying to approve does not exist'})


def decline_signup(request, id):
    if not request.user.has_perm('auth:moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        SignupSubmission.objects.filter(pk=id).delete()

        return HttpResponse('success')
    except SignupSubmission.DoesNotExist:
        return render(request, '404.html', {'message': 'Submission you are trying to decline does not exist'})
