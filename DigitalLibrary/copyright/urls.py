from django.urls import path
from . import views

app_name = 'copyright'
urlpatterns = [
    path('claim/', views.claim, name='claim'),
    path('claims_list/', views.claims_list, name='claims_list'),
    path('claim_success/', views.claim_success, name='claim_success'),
    path('remove_claim/<int:claim_id>', views.remove_claim, name='remove_claim'),
]
