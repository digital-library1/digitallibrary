from django.contrib import messages
from django.shortcuts import render, redirect

from copyright.forms import CopyrightClaimForm
from copyright.models import CopyrightClaim


def claim(request):
    if request.method == 'POST':
        form = CopyrightClaimForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.user_id = request.user.id
            model.save()
            return redirect('/copyright/claim_success')
        else:
            messages.error(request, form.errors)
    else:
        return render(request, 'copyright/claim.html', {'form': CopyrightClaimForm()})


def claims_list(request):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    list = CopyrightClaim.objects.all()
    return render(request, 'copyright/claims_list.html', {'list': list})


def claim_success(request):
    return render(request, 'copyright/claim_success.html')


def remove_claim(request, claim_id):
    if not request.user.has_perm('auth.moderate'):
        return render(request, '404.html', {'message': 'Go. Away.'})

    try:
        claim = CopyrightClaim.objects.get(pk=claim_id)
        claim.delete()
        return redirect('/copyright/claims_list')
    except CopyrightClaim.DoesNotExist:
        return render(request, '400.html', {'message': 'Invalid claim_id passed'})

