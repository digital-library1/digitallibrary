from django import forms

from copyright.models import CopyrightClaim


class CopyrightClaimForm(forms.ModelForm):
    title = forms.CharField(max_length=127, label='Title')
    body = forms.Textarea()
    telegram_alias = forms.CharField(max_length=127, label='Telegram Alias')

    class Meta:
        model = CopyrightClaim
        fields = ('title', 'body', 'telegram_alias')
