function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function HttpGet(theUrl, successCallback, failureCallback) {
    let xmlHttp = new XMLHttpRequest()
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4) {
            if (xmlHttp.status === 200)
                successCallback(xmlHttp.responseText)
            else
                failureCallback(xmlHttp.responseText)
        }
    }
    xmlHttp.open("GET", theUrl, true)
    xmlHttp.send(null)
}

function HttpPost(theUrl, body, successCallback, failureCallback) {
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4) {
            if (xmlHttp.status === 200)
                successCallback(xmlHttp.responseText)
            else
                failureCallback(xmlHttp.responseText)
        }
    }
    let csrftoken = getCookie('csrftoken')
    xmlHttp.open("POST", theUrl, true)
    xmlHttp.setRequestHeader("X-CSRFToken", csrftoken)
    xmlHttp.send(body)
}

function htmlDecode(input) {
    let doc = new DOMParser().parseFromString(input, "text/html")
    return doc.documentElement.textContent
}

function getURLParam(key) {
    const url = new URL(window.location.href)
    return url.searchParams.get(key)
}

function stopPropagation(event) {
    event.stopPropagation()
}


