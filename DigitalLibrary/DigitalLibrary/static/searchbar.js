// let searchbar_input = document.getElementById('search-bar-query')
// let observer = new MutationObserver(function (event) {
//     console.log(event)
// })
//
// observer.observe(searchbar_input, {
//     attributes: true,
//     attributeFilter: ['class'],
//     childList: false,
//     characterData: false
// })
// console.log('Observer is set.')

// setTimeout(function () {
//   searchbar_input.className = 'hello'
// }, 1000)

/* Searchbar  */
document.getElementById("search-bar-query").addEventListener('input', doThing);
let searchTimer = null

function doThing() {
    if (searchTimer != null)
        clearTimeout(searchTimer)

    searchbar_results_box = $('#search-bar-results-box')
    searchbar_results = $('#search-bar-results')
    let query = $('#search-bar-query').val()
    if (query.trim() === '') {
        searchbar_results_box.css('visibility', 'hidden');
        return;
    } else {
        searchbar_results_box.css('visibility', 'visible');
        searchbar_results.html('<p>Searching...</p>')
    }

    searchTimer = setTimeout(searchbar_search, 300)
}

function searchbar_search() {
    let query = $('#search-bar-query').val()

    searchbar_results = $('#search-bar-results')

    HttpGet('/materials/searchbar_search/' + query, response => {
        console.log('success!')
        searchbar_results.html(response)
    }, response => {
        console.log('failure!')
        searchbar_results.html(`<p>Failed to get results. Error message:</p><p>${response}</p>`)
    })
}
