
file_icon = '/static/material_icon.png'

icons = {
    'jpg': '/static/jpg_icon.png',
    'pdf': '/static/pdf_icon.png',
    'png': '/static/png_icon.png',
    'pptx': '/static/pptx_icon.png',
    'txt': '/static/txt_icon.png',
}


# noinspection PyBroadException
def getIconURL(filetype):
    try:
        filetype = filetype.lower()
        return icons[filetype]
    except Exception:
        return file_icon
