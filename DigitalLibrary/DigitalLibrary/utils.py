from uuid import UUID


def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    from datetime import datetime
    now = datetime.now()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time, datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return "%s seconds ago" % second_diff
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return "%s minutes ago" % int(round(second_diff / 60))
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return "%s hours ago" % int(round(second_diff / 3600))
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff / 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff / 30) + " months ago"
    return str(day_diff / 365) + " years ago"


def uuidJL(uuid):
    """
    Converts usual UUID representation to Java legacy UUID representation
    :rtype: uuid.UUID
    """
    # Handle UUIDs passed as a string
    if type(uuid) == str:
        uuid = UUID(uuid)

    bytes = uuid.bytes
    return UUID(bytes=bytes[0:8][::-1] + bytes[8:16][::-1])
