#!/bin/sh
# Passed environmental variables are:
#   - $APP
#   - $DOCKER_REGISTRY

cd ../../DigitalLibrary
docker build -t $APP:latest --no-cache -f ../docker/digital-library/Dockerfile .
