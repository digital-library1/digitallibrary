#!/bin/sh
# Passed environmental variables are:
#   - $APP
#   - $DOCKER_REGISTRY

docker build -t $APP:latest --no-cache -f ./Dockerfile .
